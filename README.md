# Jq Dojo

Jq is one of the most used Maestro task, because it allows powerful transformation
of json documents.

Jq: https://stedolan.github.io/jq/

Jq manual: https://stedolan.github.io/jq/manual/

Jq online: https://jqplay.org/

## How to do the exercises

You can:

- Use jqplay online, but it might be slow, because the provided file is big (and it's sent for each query)
- Install it locally, and run it as a cli: `jq '<query>' data/airports.json`

In all exercises, you must find out the right jq query that satisfies the exercise.

You can see the expected output in each `expected/exerciseXX.json`.

Bonus: if you're using jq locally and have bash installed, you can automatically check your query is
correct by updating it in each `check.sh`, and running it (not tested on Windows): `bash check.sh`.

## Exercise 01 (Level 0)

Input file: `airports.json`

Goal: format and pretty print the content on this file.

Query found: `query`

## Exercise 02 (Level 1)

Input file: `airports.json`

Goal: display first airport of the list

Query found: `query`

## Exercise 03 (Level 2)

Input file: `airports.json`

Goal: display the `size` value of the first airport of the list

Query found: `query`

## Exercise 04 (Level 2)

Input file: `airports.json`

Goal: find the total number of airports

Query found: `query`

## Exercise 05 (Level 5)

Input file: `airports.json`

Goal: find the total number of airports in Europe (EU)

Query found: `query`

## Exercise 06 (Level 6)

Input file: `airports.json`

Goal: remove all elements except "iata" and "name" in each airport. Output should be a list.

Query found: `query`

## Exercise 07 (Level 6)

Input file: `airports.json`

Goal: only keep iata and name, but present the result as a mapping: { "iata1": "name1", "iata2": "name2", ...}

Query found: `query`

## Exercise 08 (Level 2)

Input file: `sub_data.json`

Goal: check if the output code is 201

Query found: `query`

## Exercise 09 (Level 3)

Input file: `sub_data.json`

Goal: pretty print the output body as a json object

Query found: `query`

## Exercise 10 (Level 3)

Input file: `sub_data.json`

Goal: fix `data.output.headers.Length` with the actual length of the `body`

Query found: `query`

## Exercise 11 (Level 4)

Input file: `sub_data.json`

Goal add a valid Basic Authentication Authorization header in `data.output.headers.Length` with username `hello` and password `password`

Query found: `query`
