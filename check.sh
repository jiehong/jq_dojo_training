#!/bin/env bash

function check_exercise() {
  EXERCISE="$1"
  DATA="$2"
  QUERY="$3"

  printf '%s: ' "Exercise $EXERCISE"

  diff <(jq "$QUERY" "./data/$DATA" 2>&1) "./expected/exercise$EXERCISE.json" > /dev/null
  STATUS="$?"

  if [[ "$STATUS" == "0" ]]; then
    # Print in blue
    printf '\e[34m%s\e[0m\n' 'SUCCESS'
  else
    # Print in orange / brown
    printf '\e[33m%s\e[0m\n' 'TRY AGAIN'
  fi
}

check_exercise '01' 'airports.json' 'query'
check_exercise '02' 'airports.json' 'query'
check_exercise '03' 'airports.json' 'query'
check_exercise '04' 'airports.json' 'query'
check_exercise '05' 'airports.json' 'query'
check_exercise '06' 'airports.json' 'query'
check_exercise '07' 'airports.json' 'query'
check_exercise '08' 'sub_data.json' 'query'
check_exercise '09' 'sub_data.json' 'query'
check_exercise '10' 'sub_data.json' 'query'
check_exercise '11' 'sub_data.json' 'query'
